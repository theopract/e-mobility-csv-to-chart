export const options = {
    chart: {
        type: 'variablepie'
    },
    title: {
        text: 'e-Mobility charges',
        align: 'left'
    },
    subtitle: {
        text: 'Default',
        align: 'left'
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'Energy (kWh): <b>{point.y}</b><br/>' +
            'Charging duration (minutes): <b>{point.z}</b><br/>' +
			'Amount of charges: <b>{point.ac}</b><br/>'
    },
};