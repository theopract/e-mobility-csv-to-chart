function durationToHours(duration) {
    const [hours, minutes, secods] = duration.split(':').map(e => +e);
    return +parseFloat(hours + minutes/60 + secods/60/60).toFixed(2);
}

function durationToMinutes(duration) {
    const [hours, minutes, secods] = duration.split(':').map(e => +e);
    return +parseFloat(hours * 60 + minutes + secods/60).toFixed(2);
}

function getData(fileContent) {
    const props = fileContent.split('\n')[0].split(';')
    const data = fileContent.split('\n').slice(1).filter(l => l.indexOf(';') !== -1).map(line => {
        const values = line.split(';');
        const id = values[4];
        const timestamp = new Date(values[6]);
        const connectionDuration = durationToMinutes(values[10]);
        const chargeDuration = durationToMinutes(values[14]);
        const energy_kWh = +values[8];
        return {
            id,
            timestamp,
            connectionDuration,
            chargeDuration,
            energy_kWh
        }
    })
    return data.filter(e => e.chargeDuration);
}

function getTotals(data) {
	const output = new Map();
	data.forEach(e => {
		if (output.has(e.id)) {
			const { chargesAmount, connectionDuration, chargeDuration, energy_kWh } = output.get(e.id);
			output.set(e.id, {
				chargesAmount: chargesAmount + 1,
				connectionDuration: +(connectionDuration + e.connectionDuration).toFixed(2),
				chargeDuration: +(chargeDuration + e.chargeDuration).toFixed(2),
				energy_kWh: +(energy_kWh + e.energy_kWh).toFixed(2)
			});
		} else {
			output.set(e.id, {
				chargesAmount: 1,
				connectionDuration: e.connectionDuration,
				chargeDuration: e.chargeDuration,
				energy_kWh: e.energy_kWh
			});
		}
	})
	return output
}

export { getData, getTotals };