import { options } from './js/chartOptions.js';
import { getData, getTotals } from './js/dataProcessors.js'

function readData(f) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = e => {
      console.log({ fileIsRead: true, reader, f, e});
  
      const data = getData(reader.result);

      resolve(data);
    };
    reader.readAsText(f);
  });
}

window.onload = function () {
  const dropZone = document.getElementById("drop_zone");
  const menuContainer = document.getElementsByClassName("download-csv__container");

  dropZone.addEventListener("dragover", handleDropZoneDragOver, false);
  dropZone.addEventListener("drop", handleDropZoneFileSelect, false);
  dropZone.addEventListener("dragleave", handleDropZoneDragLeave);

  function handleDropZoneFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    dropZone.classList.remove('drag-over-effect');

    // files is a FileList of File objects. List some properties.
    const files = evt.dataTransfer.files; // FileList object.

    const readPromises = [];
    
	  for (let i = 0, f; (f = files[i]); i++) {
      const readPromise = readData(f);
      readPromises.push(readPromise);
    }

    Promise.all(readPromises).then(results => {
      const minTimestamp = new Date(Math.min(...results.flat().map(e => e.timestamp))).toISOString().split('T')[0];
      const maxTimestamp = new Date(Math.max(...results.flat().map(e => e.timestamp))).toISOString().split('T')[0];
      const totals = getTotals(results.flat());
      const total = [...totals.entries()].map(([id, { chargeDuration, chargesAmount, energy_kWh }]) => ({
        name: id,
        y: energy_kWh,
        z: chargeDuration,
        ac: chargesAmount
      }))
      console.log({total});
      options.subtitle.text = `Date range: ${minTimestamp} - ${maxTimestamp}`;
      options.series = [{
        minPointSize: 10,
        innerSize: '20%',
        zMin: 0,
        name: 'charges',
        data: total
      }]

        Highcharts.chart("chart-container", options);
        new gridjs.Grid({ 
          columns: ['UID', 'Energy, kWh', 'Total Duration', 'Amount of Charges'],
          data: total.map(o => Object.values(o)),
          sort: true
        }).render(document.getElementById('table-container'));
    });

  }

  function handleDropZoneDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "copy"; // Explicitly show this is a copy.
    dropZone.classList.add('drag-over-effect');
  }

  function handleDropZoneDragLeave(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    dropZone.classList.remove('drag-over-effect');
  }

  
  Highcharts.setOptions(options);

};
